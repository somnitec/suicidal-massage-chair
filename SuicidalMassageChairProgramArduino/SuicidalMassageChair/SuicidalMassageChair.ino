#define pump 6
#define mssgup 4
#define mssgdown 5
#define kneading 10
#define pounding 11
#define feet 9
#define heating1 100
#define heating2 100
#define vibration A0
#define butt A1
#define legs A2
#define arms A3
#define head A4
#define led A5
#define chairup 8
#define chairdown 7

#define topstop 3
#define botstop 2
#define blue 100
#define green 100
#define beige 100
#define grey 100
#define pink 100



int outputs[] = {pump, mssgup, mssgdown, kneading, pounding, feet, heating1, heating2, vibration, butt, legs, arms, head, led, chairup, chairdown};
int inputs[] = {topstop, botstop, blue, green, beige, grey, pink};
int arraySize;

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

bool updownon = false;
long updowntimer = 0;
bool upordown = true;
bool vibron = false;
long vibrtimer = 0;
bool airon = false;
long airtimer = 0;
int airthings[] = {butt, legs, arms, head};
int airthing = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial) {
  }


  arraySize = sizeof(outputs) / sizeof(outputs[0]);
  for (int i = 0; i < arraySize; i++)pinMode(outputs[i], OUTPUT);
  arraySize = sizeof(inputs) / sizeof(inputs[0]);
  for (int i = 0; i < arraySize; i++)pinMode(inputs[i], INPUT);


  inputString.reserve(200);
}

void loop() {
  if (stringComplete) {

    if (inputString.equals("1\n" )) {
      Serial.println("1 = pounding" );
      digitalWrite(pounding, !digitalRead(pounding));
    }
    else if (inputString.equals("2\n" )) {
      Serial.println("2= kneading" );
      digitalWrite(kneading, !digitalRead(kneading));
    }
    else if (inputString.equals("3\n" )) {
      Serial.println("3 = up/down" );
      updownon = !updownon;
    }
    else if (inputString.equals("4\n" )) {
      Serial.println("4 = vibration" );
      vibron = !vibron;
    }
    else if (inputString.equals("5\n" )) {
      Serial.println("5 = airthings" );
      airon = !airon;
    }
    else if (inputString.equals("6\n" )) {
      Serial.println("6 = led" );
      digitalWrite(led, !digitalRead(led));
    }
    else if (inputString.equals("7\n" )) {
      Serial.println("7 = feet" );
      digitalWrite(feet, !digitalRead(feet));
    }
    else if (inputString.equals("8\n" )) {
      Serial.println("8 = chairdown" );
      digitalWrite(chairdown, !digitalRead(chairdown));
    }
    else if (inputString.equals("9\n" )) {
      Serial.println("9 = chairup" );
      digitalWrite(chairup, !digitalRead(chairup));
    }
    inputString = "";
    stringComplete = false;
  }

  if (updownon ) {
    if (millis() - updowntimer > 2000) {
      if (upordown) {
        digitalWrite(mssgup, LOW);
        digitalWrite(mssgdown, HIGH);
        Serial.println("mssg moving down");
      } else {
        digitalWrite(mssgdown, LOW);
        digitalWrite(mssgup, HIGH);
        Serial.println("mssg moving up");
      }
      upordown = !upordown;
      updowntimer = millis();
    }
  } else {
    digitalWrite(mssgup, LOW);
    digitalWrite(mssgdown, LOW);
  }

  if ( vibron) {
    if (millis() - vibrtimer > 500) {
      digitalWrite(vibration, !digitalRead(vibration));
      vibrtimer = millis();
      Serial.println("vibration on/off");
    }

  } else digitalWrite(vibration,LOW);
  if (airon) {
    digitalWrite(pump, HIGH);
    if (millis() - airtimer > 3000) {
      airtimer = millis();
      digitalWrite(airthings[(airthing - 1) % 4], LOW);
      digitalWrite(airthings[airthing], HIGH);
      Serial.print("airthing: ");
      Serial.println(airthing);
      airthing=(airthing+1)%4;
    }

  } else{
    digitalWrite(pump, LOW);
    digitalWrite(airthings[0], LOW);
    digitalWrite(airthings[1], LOW);
    digitalWrite(airthings[2], LOW);
    digitalWrite(airthings[3], LOW);
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
